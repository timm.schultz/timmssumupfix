#!/usr/bin/env python3
#-*- coding: utf-8 -*-

"""A tool for "fixing" the SUMup snow density subdataset.

The SUMup snow density subdataset is a huge collection of snow and firn
density data and a great tool for all interested in snow and firn. It
was assembled by Lora Koenig and Lynn Montgomery:

Lora Koenig and Lynn Montgomery. 2019. Surface Mass Balance and Snow
Depth on Sea Ice Working Group (SUMup) snow density subdataset,
Greenland and Antarcitca, 1950-2018. doi10.18739/A26D5PB2S.

In my opinion the idea is really great, but the dataset has one huge
flaw which makes working with it a little hard. All datapoints are
stored in one very long list. And profiles are not numbered. So
individual profiles (or single point measurements) can't be accessed.
Furthermore a lot of the data is redundant as every information of
the metadata is stored for every datapoint.

This scripts aim is to analyse the original netCDF-file and to write a
new one making use of netCDF4's "variable length" datatype. In this way
individual profiles are stored as one row of the new data table each.
This makes profiles accessible and saves quite some disk space.

The script is capable of downloading the original dataset from the "NSF
Arctic Data Center", to sort out some NaN-values, identify profiles,
reorganizing the data and to save them to a netCDF-file. As the analysis
of the original data in theory only have to be done once, the script can
also read information abouth the "fix" from a local file and apply these
information to the dataset.

For more information see the README file of this project.

ALSO READ THE README OF LORA KOENIG AND LYNN MONTGOMERY, AVAIABLE HERE:
https://cn.dataone.org/cn/v2/resolve/urn:uuid:7785ea08-6394-4ae9-9a8d-bdb45d958e7f

All data are avaiable online at the "NSF Arctic Data Center".
https://arcticdata.io/catalog/view/doi:10.18739/A26D5PB2S

Functions
---------
sumup_download
    download the original SUMup snow density subdataset
read_sumup
    read the SUMup snow density subdataset's netCDF-file
read_fix_info
    read information about the "fix"
write_fix_info
    write previously generated information about the dataset to a file
find_nan
    find nan values in "critical" datafields
find_profiles
    indentify individual profiles and singe point measurements
write_netcdf
    write a new reorganized netCDF-file

Raises
------
ImportError
    import error is raised if "netCDF4" netCDF-API is not available
"""


import os
import urllib.request

try:
    import netCDF4
except:
    raise ImportError('The fix-script depends on netCDF4. '
                      'It seems like it is not available.')

try:
    import numpy as np
except:
    np = None


# constants
SUMUP_URL = (
    """https://arcticdata.io/metacat/d1/mn/v2/object/"""
    """urn%3Auuid%3Afc6800a1-63f2-4784-a9c2-f5ec92c70659"""
)
FIX_INFO_FILE = 'fix_info.dat'



def sumup_download():
    """Download of the SUMup snow density subdataset.

    "sumup_download" checks if the file "sumup_density_2019.nc" exists
    in the current working directory. This is the standard name of the
    SUMup snow density subdataset.
    If the file is not present the function downloads it from the URL
    given via the constant "SUMUP_URL".

    Returns
    -------
    sumup_data_file : str
        name of the SUMup snow density subdataset netCDF-file
    """
    
    sumup_data_file = 'sumup_density_2019.nc'
    
    if not os.path.isfile(sumup_data_file):
        print('Downloading the SUMup snow density subdataset from '
            'NSF Arctic Data Center...')
        urllib.request.urlretrieve(SUMUP_URL, sumup_data_file)
        print('... done.\n')

    else:
        print('The SUMup snow density subdataset exists locally.\n')

    return sumup_data_file


def read_sumup(data_file):
    """Read netCDF-file into loacl dictionary.

    The function "read_sumup" reads a netCDF-file and saves it's fields
    in form of a dictionary. This allows to further manipulate the
    data.

    Parameters
    ----------
    data_file : str
        location and / or name of a netCDF-file

    Returns
    -------
    data : dict
        dictionary holding the data stored in the netCDF-file
    """

    data = {}
    dataset = netCDF4.Dataset(data_file, 'r')
    for key in dataset.variables.keys():
        data[key] = dataset.variables[key][:]
    dataset.close()

    return data


def read_fix_info(file_name):
    """Read profile indices from a file.

    This function reads a text file holding integers and returns them as
    lists. This integers are indices which can be used to fix the SUMup
    snow density subdataset and are organized in the following form.
    The first line of the file holds space separated integers indicating
    the beginning and end of one profile in the dataset.
    The second line also holds space separated integers indicating the
    positions of datapoints which can't be used because critical
    information is missing in the dataset.

    Parameters
    ----------
    file_name : str
        location and / or name of a file holding fix information

    Returns
    -------
    profile_indices : list
        list of indices indicating the beginning and end of a profile
    nan_indices : list
        list of indices indicating the position of NaN values
    """
    
    inp = open(file_name, 'r')
    profile_indices = [int(p) for p  in inp.readline().split(' ')]
    nan_indices = [int(p) for p  in inp.readline().split(' ')]
    inp.close()

    return profile_indices, nan_indices


def write_fix_info(profile_indices, nan_indices):
    """Write profile indices to an output file.

    The function writes to lists, "profile_indices" and "nan_indices",
    to a text file. These lists may hold information about the SUMup
    snow density subdataset. For futher inoformation also see the
    function "read_fix_info".

    Parameters
    ----------
    profile_indices : list
        list of indices indicating the beginning and end of a profile
    nan_indices : list
        list of indices indication the position of NaN values

    Returns
    -------
    FIX_INFO_FILE : str
        name of the output file defined via the constant FIX_INFO_FILE
    """

    tostring = lambda l: ' '.join(str(int(ll)) for ll in l) + '\n'

    out = open(FIX_INFO_FILE, 'w')
    out.write(tostring(profile_indices))
    out.write(tostring(nan_indices))
    out.close()

    return FIX_INFO_FILE


def find_nan(data_file):
    """NaNs in critical fields.

    "find_nan" finds NaN-values inside the SUMup snow density
    subdataset. It reads the netCDF-file given via the parameter
    "data_file" and searches for the NaN-Value "-9999" in certain
    fields. These fields are "Density", "Midpoint", "Latitude" and
    "Longitude". If any of these information is missing for a datapoint
    it can't be used further in a meaningful way.
    The function returns the indices of NaN-values in form of a list.

    If "numpy" is available the function will use it to speed up the
    search. Otherwise the task will be done in pure python which takes a
    little more time.

    Parameters
    ----------
    data_file : str
        location and / or name of the netCDF-file holding the data

    Returns
    -------
    nan : list
        list of integers indicating position of NaN-values
    """

    data = read_sumup(data_file)

    # key definitoins
    keys = ['Density', 'Midpoint', 'Latitude', 'Longitude']

    # numpy method -> fast
    if np:
        nan = np.array([data[key] for key in keys])
        nan = np.where(nan == -9999)[1]
        nan = np.unique(nan)
    
    # pure python method -> slow
    else:
        nan = [data[key] == -9999 for key in keys]
        nan = [any(n) for n in zip(*nan)]
        nan = [n for n in range(len(nan)) if nan[n] == True]

    return nan


def find_profiles(data_file):
    """Identify profiles in the data.

    The function will find individual profiles inside the SUMup snow
    density subdataset. To do so, it will combine two methods.

    First the field "Midpoint" of the original dataset is evaluated.
    Assuming that all profiles are sorted in a way the depth is always
    incresing, a new profile can be found if the gradient of the depth
    gets negative from one data point to the next.

    As the dataset includes a lot of single point measurements and not
    all measurements start at the same depth, not all "profiles" can be
    found using the first method. For this reason a hash over the
    metadata "Date", "Latitude", "Longitude", "Method", "Citation" and
    "SDOS_flag" is caluclated for each datapoint. Same hashes indicate
    that the datapoints are part of one profile. But as often more than
    one profile exists with the same metadata this method has to be
    combined with the first one.

    If "numpy" is available the function will use it to speed up the
    search. Otherwise the task will be done in pure python which takes a
    little more time.

    Parameters
    ----------
    data_file : str
        location and / or name of the netCDF-file holding the data

    Returns
    -------
    profiles : list
        list of integers indicating the beginning and end of a profile
    """

    data = read_sumup(data_file)

    p = data['Midpoint'][:]
    meta_keys = ['Date', 'Latitude', 'Longitude', 'Method',
        'Citation', 'SDOS_Flag']

    # numpy method -> fast
    if np:
        # gradient of midpoints
        p = [0] + (np.where(np.diff(p) <= 0.0)[0] + 1).tolist()

        # hash over metadata
        h = np.array([data[key] for key in meta_keys])
        h = np.array([hash(h[:,n].tobytes()) for n in range(h.shape[1])])
        h = np.unique(h, return_index=True)[1].tolist()

    # pure python method -> slow
    else:
        # gradient of midpoints
        p = [p[n] - p[n-1] for n in range(1, len(p))]
        p = [0] + [(n + 1) for n in range(len(p)) if p[n] <= 0.0]

        # hash over metadata
        h = [data[key] for key in meta_keys]
        h = [hash(str(hh)) for hh in zip(*h)]
        h = [h.index(hh) for hh in set(h)]

    # combination of both methods
    profiles = sorted(list(set(p + h)))

    return profiles


def write_netcdf(data_file, pind, nind, name='timms_sumup_density_2019.nc'):
    """Store profiles in a netCDF file."""

    # key definitions
    meta_keys = ['Date', 'Latitude', 'Longitude', 'Elevation',
        'SDOS_Flag', 'Method', 'Citation']

    data_keys = ['Density', 'Start_Depth', 'Stop_Depth',
        'Midpoint', 'Error']

    # orifinal data
    data = read_sumup(data_file)

    # translation of NaN indices to a boolean list
    nan = [True] * len(data['Density'])
    for i in nind:
        nan[i] = False

    # new dictionary
    new = {}
    for key in data.keys():
        new[key] = []

    # loop over all profiles
    for n in range(1, len(pind)):
        check = nan[pind[n-1]:pind[n]]

        if any(check):

            # metadata
            for key in meta_keys:
                 dat = data[key][pind[n-1]:pind[n]][check][0]
                 new[key].append(dat)

            # data
            for key in data_keys:
                 dat = data[key][pind[n-1]:pind[n]][check]
                 new[key].append(dat)
    

    # new netCDF with variable length data type
    dataset = netCDF4.Dataset(name, 'w')
    dataset.createDimension('profile', (len(new['Date'])))
    vlen = dataset.createVLType('f4', 'data')

    # metadata
    dtypes = ['u4', 'f4', 'f4', 'f4', 'u2', 'u2', 'u4']
    for n in range(len(meta_keys)):
        v = dataset.createVariable(meta_keys[n], dtypes[n], ('profile'))
        v[:] = new[meta_keys[n]]

    # data
    for key in data_keys:
        v = dataset.createVariable(key, vlen, ('profile'))
        for n in range(len(new[key])):
            v[n] = new[key][n]

    dataset.close()


if __name__ == "__main__":
    
    # get and read the original dataset
    sumup_data_file = sumup_download()

    # fix information from file
    if os.path.isfile(FIX_INFO_FILE):

        print('fix information from local file...')
        profiles, nan = read_fix_info(FIX_INFO_FILE)

    # fix information from original dataset
    else:

        print('find NaN values...')
        nan = find_nan(sumup_data_file)

        print('profile identification...')
        profiles = find_profiles(sumup_data_file)

        write_fix_info(profiles, nan)

    # write the fixed dataset
    print('new netCDF file...')
    write_netcdf(sumup_data_file, profiles, nan)

    print('... done!')
